<?php
/*
Plugin Name: PHP-Ref
Plugin URI: https://github.com/digitalnature/php-ref
Description: Debugging in PHP, the beautiful way
Version: 1.0
Author: digitalnature
Author URI: https://github.com/digitalnature/php-ref
*/

require_once('ref.php');